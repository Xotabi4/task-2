
class DataBase extends React.Component {

    constructor(props) {
        super(props);
        this.test = {
            startTime: "09:00",
            endTime: "17:30",
            requests :[
                {
                    requestDate:"2011-03-17 10:17:06",
                    name:"emp001",
                    meetingDate:"2011-03-21 09:00:00",
                    meetingDuration:"2"
                },
                {
                    requestDate:"2011-03-16 12:34:56",
                    name:"emp002",
                    meetingDate:"2011-03-21 09:00:00",
                    meetingDuration:"2"
                },
                {
                    requestDate:"2011-03-17 11:23:45",
                    name:"emp004",
                    meetingDate:"2011-03-22 16:00:00",
                    meetingDuration:"1"
                },
                {
                    requestDate:"2011-03-16 09:28:23",
                    name:"emp003",
                    meetingDate:"2011-03-22 14:00:00",
                    meetingDuration:"2"
                },
                {
                    requestDate:"2011-03-15 17:29:12",
                    name:"emp005",
                    meetingDate:"2011-03-21 16:00:00",
                    meetingDuration:"3"
                }
            ]
        };
        this.addAll = this.addAll.bind(this);
        this.sendValue = this.sendValue.bind(this);
        this.createCalendar = this.createCalendar.bind(this);
        this.time={
            startTime: "09:00",
            endTime: "17:30"
        };
        this.calendar=[];
        this.state = {
            dataBase:{
                meetings: []
            },
            calendar:{
            },
            insert:false
        };

    }

    componentDidMount() {
        $.post("http://localhost:3000/getAll", function(response){
            this.setState({dataBase:{meetings: response}});
        }.bind(this));
    }

    addAll(){
        $.ajax({
            type: "POST",
            url: "http://localhost:3000/addAll",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(this.test)
        }).done(function (response) {
            this.setState({dataBase:{meetings:response}});
        }.bind(this));
    }

    sendValue(newMeeting) {
        newMeeting.requestDate = moment().format("YYYY-MM-DD HH:mm:ss");
        var meeting={
            startTime: "09:00",
            endTime: "17:30",
            requests: []
        };
        meeting.requests.push(newMeeting);
        $.ajax({
            type: "POST",
            url: "http://localhost:3000/add",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(meeting)
        }).done(function(response){
            this.setState({dataBase:{meetings:response}});
        }.bind(this));
    }

    createCalendar() {
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "http://localhost:3000/createCalendar",
            dataType: "json",
        }).done(function(response){
            this.setState({calendar: response});
            this.setState({insert:true});
        }.bind(this));
    }

    render() {
        return (
            <div className="row row-content">
                <div className="col-xs-3">
                    <p style={{padding: 5+"px"}}></p>
                    <NewMeeting sendValue={this.sendValue}/>
                    <MeetingList meetings={this.test.requests} format={true}/>
                    <p><input type="button" className="btn btn-primary" value="Send request" onClick={this.addAll}></input></p>
                </div>
                <div className="col-xs-3">
                    <p style={{padding: 5+"px"}}></p>
                    <MeetingList meetings={this.state.dataBase.meetings} format={true}/>
                </div>
                <div className="col-xs-4 col-xs-offset-1">
                    <p style={{padding: 5+"px"}}></p>
                    <p>Work time : {this.time.startTime} - {this.time.endTime}</p>
                    <p><input type="button" className="btn btn-primary" value="Create calendar" onClick={this.createCalendar}></input></p>
                    {this.state.insert ?
                        <CalendarList calendar={this.state.calendar} /> :
                        null
                    }
                </div>
            </div>
        )
    }
}

class NewMeeting extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            requestDate: "",
            name: "",
            meetingDate: "",
            meetingDuration: ""
        };
    }

    add() {
        this.props.sendValue(this.state);
        this.state={
            requestDate: "",
            name: "",
            meetingDate: "",
            meetingDuration: ""}
    }
    handleChange(name, e) {
        var change = {
        };
        change[name] = e.target.value;
        this.setState(change);
    }

    render() {
        return(
            <div>
                <form>
                    <label for="name">Name</label>
                    <p><input onChange={this.handleChange.bind(this, 'name')} className="form-control" id="name" type="text" value={this.state.name} ></input></p>
                    <label for="meetingDate">Meeting Date</label>
                    <p><input onChange={this.handleChange.bind(this, 'meetingDate')} type="text" id="meetingDate" value={this.state.meetingDate}  className="form-control" ></input></p>
                    <label for="meetingDuration">Meeting Duration</label>
                    <p><input onChange={this.handleChange.bind(this, 'meetingDuration')} type="text" id="meetingDuration" value={this.state.meetingDuration}  className="form-control" ></input></p>
                    <p><input type="button" className="form-control btn btn-primary" value="Add request" onClick={this.add.bind(this)}></input></p>
                </form>
            </div>
        )
    }
}

class CalendarList extends React.Component{
    render(){
        var calendar = this.props.calendar;
        var keyList=Object.keys(this.props.calendar).map(function(key) {
            return <div key={key}>{key}
                <Calendar meetings={calendar[key]}/></div>
            }
        );
        return(
            <ul className="list-unstyled">
                {keyList}
            </ul>
        )
    }
}
class Calendar extends React.Component{
    render(){
        var meeting=this.props.meetings.map(meeting=>
            <Meeting key={index++} meeting={meeting} format={false}/>
        );
        return(
            <li>
                {meeting}
            </li>
        )
    }
}
var index = 0;
class MeetingList extends React.Component{
    render() {
        var meeting = this.props.meetings.map(meeting =>
            <Meeting key={index++} meeting={meeting} format={this.props.format}/>
        );
        return (
            <ul className="list-unstyled">
                {meeting}
            </ul>
        )
    }

}
class Meeting extends React.Component{
    render() {
        return (
            <div>
                {}
                {this.props.format?
                    <li>
                        <p>{this.props.meeting.requestDate} {this.props.meeting.name}</p>
                        <p>{this.props.meeting.meetingDate} - {this.props.meeting.meetingDuration}</p>
                    </li>
                    :
                    <p>{moment(new Date(this.props.meeting.meetingDate)).format("HH:mm")} - {this.props.meeting.meetingDuration} {this.props.meeting.name}</p>
                }
            </div>
        )
    }
}
class App extends React.Component{
    render(){
        return(
            <div className="container">
                <DataBase/>
            </div>
        )
    }
}
ReactDOM.render(
    <App />,
    document.getElementById('react')
);