package by.stas.repository;

import by.stas.model.Meeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;


@Repository
public interface MeetingRepository extends JpaRepository<Meeting, Integer> {

    @Query("select u from Meeting u where u.meetingDate=:meetingDate " +
            "order by u.meetingTime")
    List<Meeting> findAllByMeetingDateByOrderByMeetingTime(@Param("meetingDate") LocalDate meetingDate);

    @Query("select u.meetingDate from Meeting u order by u.meetingDate")
    Set<LocalDate> findAllMeetingDates();

    @Query("select u from Meeting u where u.meetingDate>=:startDate and u.meetingDate<=:endDate")
    List<Meeting> findAllMeetingsBetweenDates(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Query("select u from Meeting u order by u.meetingDate")
    List<Meeting> findAllOrderByMeetingDate();

    @Query("select case when (count(u) > 0) then false else true end " +
            "from Meeting u where u.meetingDate=:meetingDate and" +
            "(u.meetingTime>=:startTime or u.meetingDuration>:startTime) and " +
            "(u.meetingDuration<=:endTime or u.meetingTime<:endTime)")
    boolean checkTime(@Param("meetingDate") LocalDate meetingDate,
                      @Param("startTime") LocalTime startTime,
                      @Param("endTime") LocalTime endTime);
}