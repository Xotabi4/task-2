package by.stas.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalTime;
import java.util.List;

public class RequestDTO {

    @JsonFormat(pattern = "HH:mm")
    private LocalTime startTime;

    @JsonFormat(pattern = "HH:mm")
    private LocalTime endTime;

    private List<MeetingDTO> requests;

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public List<MeetingDTO> getRequests() {
        return requests;
    }

    public void setRequests(List<MeetingDTO> requests) {
        this.requests = requests;
    }

    public RequestDTO() {

    }

    public RequestDTO(LocalTime startTime, LocalTime endTime, List<MeetingDTO> requests) {

        this.startTime = startTime;
        this.endTime = endTime;
        this.requests = requests;
    }
}