package by.stas.service;

import by.stas.dto.MeetingDTO;
import by.stas.model.Meeting;
import by.stas.repository.MeetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;


@Service
public class RequestService {

    @Autowired
    MeetingRepository meetingRepository;

    @Autowired
    MeetingService meetingService;


    public Map<LocalDate, List<MeetingDTO>> createCalendar(){
        Set<LocalDate> meetingDates = meetingRepository.findAllMeetingDates();
        Map<LocalDate, List<Meeting>> calendar = new TreeMap<>();
        meetingDates.forEach(meetingDate -> calendar.put(
                meetingDate, meetingRepository.findAllByMeetingDateByOrderByMeetingTime(meetingDate)));
        return createDTOMap(calendar);
    }

    private Map<LocalDate, List<MeetingDTO>> createDTOMap(Map<LocalDate, List<Meeting>> sortedMap){
        Map<LocalDate, List<MeetingDTO>> finalMap = new TreeMap<>();
        sortedMap.keySet().forEach(key -> {
            finalMap.put(key, new ArrayList<>());
            sortedMap.get(key).forEach(temp -> finalMap.get(key).add(meetingService.getDTOFromMeeting(temp)));
        });
        return finalMap;
    }

    public List<MeetingDTO> createDTOList(List<Meeting> meetingList){
        List<MeetingDTO> meetingDTOs = new ArrayList<>();
        meetingList.forEach(temp -> meetingDTOs.add(meetingService.getDTOFromMeeting(temp)));
        return meetingDTOs;
    }
}