package by.stas.service;

import by.stas.dto.MeetingDTO;
import by.stas.dto.RequestDTO;
import by.stas.model.Meeting;
import by.stas.model.User;
import by.stas.repository.MeetingRepository;
import by.stas.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class MeetingService {

    @Autowired
    MeetingRepository meetingRepository;

    @Autowired
    UserRepository userRepository;

    public Meeting save(Meeting meeting) {
        return meetingRepository.saveAndFlush(meeting);
    }

    public List<Meeting> getAllMeetings() {
        return meetingRepository.findAllOrderByMeetingDate();
    }


    public Meeting save(MeetingDTO meetingDTO, LocalTime startTime, LocalTime endTime) {
        Meeting meeting = getMeetingFromDTO(meetingDTO);
        if(meeting.getMeetingTime().isBefore(startTime.minusNanos(1)) || meeting.getMeetingDuration().isAfter(endTime.plusNanos(1)))
            return meeting;
        boolean check = meetingRepository.checkTime(
                meeting.getMeetingDate(),
                meeting.getMeetingTime(),
                meeting.getMeetingDuration());
        if (!check)
            return meeting;
        return save(meeting);
    }

    public void saveAll(RequestDTO requestDTO) {
        List<MeetingDTO> list = requestDTO.getRequests();
        list.sort((o1, o2) -> o1.getRequestDate().compareTo(o2.getRequestDate()));
        list.forEach(temp->save(temp, requestDTO.getStartTime(), requestDTO.getEndTime()));
    }

    public Meeting getMeetingFromDTO(MeetingDTO meetingDTO) {
        User user = userRepository.findByName(meetingDTO.getName());
        if (user == null)
            user = new User(meetingDTO.getName());
        LocalDateTime requestDate = meetingDTO.getRequestDate();
        LocalDateTime meetingDate = meetingDTO.getMeetingDate();
        LocalTime meetingTime = meetingDate.toLocalTime();
        LocalTime meetingDuration = meetingTime.plusHours(Integer.parseUnsignedInt(meetingDTO.getMeetingDuration()));
        Meeting meeting = new Meeting(requestDate, meetingDate.toLocalDate(), meetingDate.toLocalTime(), meetingDuration, user);
        return meeting;
    }

    public MeetingDTO getDTOFromMeeting(Meeting meeting) {
        return new MeetingDTO(
                meeting.getRequestDate(),
                meeting.getUser().getName(),
                LocalDateTime.of(meeting.getMeetingDate(), meeting.getMeetingTime()),
                meeting.getMeetingDuration().format(DateTimeFormatter.ofPattern("HH:mm")));
    }
}