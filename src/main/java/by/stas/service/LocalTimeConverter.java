package by.stas.service;


 import javax.persistence.AttributeConverter;
 import javax.persistence.Converter;

 @Converter(autoApply = true)
 public class LocalTimeConverter implements AttributeConverter<java.time.LocalTime, java.sql.Time> {

     @Override
     public java.sql.Time convertToDatabaseColumn(java.time.LocalTime attribute) {

         return attribute == null ? null : java.sql.Time.valueOf(attribute);
     }

     @Override
     public java.time.LocalTime convertToEntityAttribute(java.sql.Time dbData) {

         return dbData == null ? null : dbData.toLocalTime();
     }
 }