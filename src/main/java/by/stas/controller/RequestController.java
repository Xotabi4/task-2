package by.stas.controller;

import by.stas.dto.MeetingDTO;
import by.stas.dto.RequestDTO;
import by.stas.repository.MeetingRepository;
import by.stas.service.MeetingService;
import by.stas.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class RequestController {

    @Autowired
    RequestService requestService;

    @Autowired
    MeetingService meetingService;

    @Autowired
    MeetingRepository meetingRepository;

    @RequestMapping(value = "/createCalendar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Map<LocalDate, List<MeetingDTO>> sort(){
        return requestService.createCalendar();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<MeetingDTO> add(@RequestBody RequestDTO requestDTO){
        meetingService.saveAll(requestDTO);
        return requestService.createDTOList(meetingService.getAllMeetings());
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.POST)
    public List<MeetingDTO> getAll(){
        return requestService.createDTOList(meetingService.getAllMeetings());
    }

    @RequestMapping(value = "/addAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<MeetingDTO> addAll(@RequestBody RequestDTO requestDTO){
        meetingService.saveAll(requestDTO);
        return requestService.createDTOList(meetingService.getAllMeetings());
    }

}