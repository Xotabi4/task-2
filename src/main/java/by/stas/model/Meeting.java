package by.stas.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name = "meetings")
public class Meeting {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "requestDate")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime requestDate;

    @Column(name = "meetingDate")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate meetingDate;

    @Column(name = "meetingTime")
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime meetingTime;

    @Column(name = "meetingDuration")
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime meetingDuration;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_name")
    private User user;

    public Integer getId() {
        return id;
    }

    public Meeting() {
    }

    public LocalDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public LocalDate getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(LocalDate meetingDate) {
        this.meetingDate = meetingDate;
    }

    public LocalTime getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(LocalTime meetingTime) {
        this.meetingTime = meetingTime;
    }

    public LocalTime getMeetingDuration() {
        return meetingDuration;
    }

    public void setMeetingDuration(LocalTime meetingDuration) {
        this.meetingDuration = meetingDuration;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Meeting(LocalDateTime requestDate, LocalDate meetingDate, LocalTime meetingTime, LocalTime meetingDuration, User user) {

        this.requestDate = requestDate;
        this.meetingDate = meetingDate;
        this.meetingTime = meetingTime;
        this.meetingDuration = meetingDuration;
        this.user = user;
    }
}